import os
import time
import threading


# FUNCION
##########
def ping_ip(ip_address):
    '''
    La funcion toma como agumento 'ip_address' y envia una consulta ping via cmd.
    INPUT:  *ip_address: direccion ip a pingear
    OUTPUT: 0/1
    '''
    output = os.system("ping " + ip_address)
    time.sleep(0.5)
    print('Ping {0}: {1}'.format(ip_address, output) )
    return



# MAIN - MULTITHREADING
########################
print("MULTITHREADING PROCESSING!")
ti = time.time()

# Creo los threads
t1 = threading.Thread( target=ping_ip, args=('8.8.8.8',) )
t2 = threading.Thread( target=ping_ip, args=('www.google.com',) )
t3 = threading.Thread( target=ping_ip, args=('127.0.0.1',) )

# Inicializo los threads
t1.start()
t2.start()
t3.start()

# Aguardo a que finalicen
t1.join()
t2.join()
t3.join()
tf = time.time()

print('Timer:', tf-ti)



# MAIN - SERIAL PROCESSING
###########################
ips = [ '8.8.8.8', 'www.google.com', '127.0.0.1' ]
ti = time.time()
print('\nSERIAL PROCESSING!')
for n in ips:
    ping_ip( n )
tf = time.time()
print('Timer:', tf-ti)
