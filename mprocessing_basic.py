import time
import multiprocessing
from multiprocessing import Pool



def cuadrado(n):
    res = n*n
    return res


num = range(100)


if __name__ == '__main__':
    t2 = time.time()
    pool = multiprocessing.Pool()
    result = pool.map(cuadrado, num)
    print('Timer:', time.time() - t2)
